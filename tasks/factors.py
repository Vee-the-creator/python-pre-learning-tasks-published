def factors(number):
    # ==============
    # Your code here
    myfactors = []
    #count = 0
    for i in range (1, number):
        if(number % i == 0):
            if(i != 1):
                myfactors.append(i)
            #else:
                # print("Number is 0 or the number entered")            

    return myfactors
   
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “[]” (an empty list) to the console
