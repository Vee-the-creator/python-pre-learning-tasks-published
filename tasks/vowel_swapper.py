def vowel_swapper(string):
    # ==============
    # Your code here
   
    vowel_mappings = {"a": "4", "e": "3", "o": "0", "i": "!", "u": "|_|" }

    for i in string:
        if(i.lower() in vowel_mappings.keys()):
            string = string.replace(i, vowel_mappings[i.lower()])
    
    return string

    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console