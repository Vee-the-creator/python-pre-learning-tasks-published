def calculator(a, b, operator):
    # ==============
    # Your code here
    answer = 0

    if operator == "+":
        answer = a + b
    elif operator == "-":
        answer = a - b
    elif operator == "*":
        answer = a * b
    elif operator == "/":
        answer = int(a / b)
    else:
        print("Invalid operator given")


    return answer
    # ==============

print(calculator(2, 4, "+")) # Should print 6 to the console
print(calculator(10, 3, "-")) # Should print 7 to the console
print(calculator(4, 7, "*")) # Should print 28 to the console
print(calculator(100, 2, "/")) # Should print 50 to the console
